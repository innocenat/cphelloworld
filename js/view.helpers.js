/**
 * Created by innocenat on 26/07/15.
 */

Handlebars.registerHelper('distance_decode', function(distance) {
    if (+distance < -2)
        return 'Sun';
    switch (+distance) {
        case -2: return 'Mercury';
        case -1: return 'Venus';
        case 0: return 'Earth';
        case 1: return 'Mars';
        case 2: return 'Jupiter';
        case 3: return 'Saturn';
        case 4: return 'Uranus';
        case 5: return 'Neptune';
        case 6:case 7:case 8: return 'Pluto';
        default: return 'Kuniper belt';
    }
});
Handlebars.registerHelper('internet_decode', function(internet) {
    if (internet < 0) internet = 0;
    return Math.round(Math.pow(1.25, internet*2.25)-1) + " Mbps";
});
Handlebars.registerHelper('food_decode', function(food) {
    if (food < 0) food = 0;
    return Math.round(Math.pow(1.8, food)-1) + " tonnes";
});
Handlebars.registerHelper('power_decode', function(power) {
    if (power < 0) power = 0;
    return Math.round(Math.pow(1.15, power*4)-1) + " Gigawatts";
});

Handlebars.registerHelper('game_status_text', function(status) {
    switch (+status) {
        case 0: return 'Pending';
        case 1: return 'Ongoing';
        case 2: return 'Finalized';
        default: return 'Unknown';
    }
});
Handlebars.registerHelper('time_text', function(time) {
    if (+this.status == 0) return 'Pending';
    var diff = +time - moment().unix();
    if (diff <= 0) return "Time's up";
    if (diff < 60) return diff + "s";
    return Math.floor(diff/60) + "m " + diff%60 + "s";
});

Handlebars.registerHelper('calc_left_money', function() {
    return this.money - this.spent;
});
Handlebars.registerHelper('abs', function(v) {
    return Math.abs(v);
});
Handlebars.registerHelper('if_overspent', function(options) {
    if (this.money < this.spent)
        return options.fn(this);
    return '';
});
Handlebars.registerHelper('format_number', function(num) {
    if (num > 0) return '+' + num;
    else return num;
});

Handlebars.registerHelper('if_eq', function(v1, v2, options) {
    if(v1 === v2) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_pos', function(v1, options) {
    if(v1 >= 0) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('if_neg', function(v1, options) {
    if(v1 < 0) {
        return options.fn(this);
    }
    return options.inverse(this);
});

Handlebars.registerHelper('humanize_time', function(time) {
    return moment.unix(time).fromNow();
});


function isFinished(time) {
    return time < moment().unix();
}
