<?php

define_admin_api('game_info', function() {
    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    $info = [
        'id' => $game_id,
        'round' => $bean->round,
        'distance' => $bean->distance,
        'power' => $bean->power,
        'hp' => $bean->hp,
        'food' => $bean->food,
        'internet' => $bean->internet,
        'money' => $bean->money,
    ];

    $user_beans = $bean->ownPlayerList;
    $_ = reset($user_beans);
    $_ = end($user_beans);

    $user = [];
    foreach ($user_beans as $user_bean) {
        $user[] = [
            'id' => $user_bean->id,
            'username' => $user_bean->username,
            'password' => $user_bean->password,
            'money'    => $user_bean->money,
        ];
    }

    $info['players'] = $user;

    Flight::json($info);
});

define_admin_api('get_update', function() {
    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    $info = [
        'game_id' => $bean->id,
        'round' => $bean->round,
        'status' => $bean->status,
        'finished_time' => $bean->finished_time,
    ];

    Flight::json($info);
});

define_admin_api('create_game', function() {
    $name = Flight::request()->query->name;
    if (empty($name) || mb_strlen($name) > 200)
        Flight::halt(400, 'ERROR: Invalid name');

    $bean = R::dispense('game');
    $bean->name = $name;

    $bean->distance = 0;
    $bean->hp       = 0;
    $bean->power    = 0;
    $bean->food     = 0;
    $bean->internet = 0;
    $bean->money    = 0;
    $bean->round    = 1;

    $bean->status = 0;
    $bean->finished_time = 0;

    $id = R::store($bean);

    Flight::json(['id' => $id]);
});

define_admin_api('list_game', function() {
    $games = R::findAll('game');

    $active_game = get_global_setting('current_game', -1);

    $game_ret = [];
    foreach ($games as $game) {
        $game_ret[] = ['id' => $game->id, 'name' => $game->name, 'active' => $active_game == $game->id];
    }
    Flight::json($game_ret);
});

define_admin_api('reset_game', function() {
    $game_id = Flight::request()->query->game;
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    R::exec('DELETE FROM attempt WHERE game_id = ? ', [$bean->id]);

    $bean->distance = 0;
    $bean->hp       = 0;
    $bean->power    = 0;
    $bean->food     = 0;
    $bean->internet = 0;
    $bean->money    = 0;
    $bean->round    = 1;

    $bean->status = 0;
    $bean->finished_time = 0;

    R::store($bean);

    Flight::json(true);
});

define_admin_api('set_active_game', function() {
    $game_id = Flight::request()->query->game;
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    set_global_setting('current_game', $game_id);

    Flight::json(true);
});

define_admin_api('destroy_game', function() {
    $game_id = Flight::request()->query->game;
    $bean = R::load('game', $game_id);
    if ($game_id == get_global_setting('current_game', -1) || $bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

   R::trash($bean);

    Flight::json(true);
});

define_admin_api('update_game', function() {
    $game_id = Flight::request()->query->game;
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    $q = Flight::request()->query;
    $bean->hp = $q->hp;
    $bean->internet = $q->internet;
    $bean->food = $q->food;
    $bean->money = $q->money;
    $bean->power = $q->power;
    $bean->distance = $q->distance;

    R::store($bean);

    Flight::json(true);
});

define_admin_api('create_player', function() {
    $username = trim(Flight::request()->query->username);
    $password = Flight::request()->query->password;
    $game_id = Flight::request()->query->game;
    if (empty($username) || mb_strlen($username) > 200 || empty($password) || mb_strlen($password) > 200)
        Flight::halt(400, 'Error! Invalid username or password');

    if (R::findOne('player', 'username = ?', [$username]))
        Flight::halt(403, 'Error! Duplicated username.');

    $game = R::load('game', $game_id);
    if ($game->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    $bean = R::dispense('player');
    $bean->username = trim($username);
    $bean->password = $password;
    $bean->money    = 0;
    $bean->game     = $game;
    $id = R::store($bean);

    Flight::json(['id' => $id]);
});

define_admin_api('update_player', function() {
    $user_id = Flight::request()->query->user_id;

    $bean = R::load('player', $user_id);
    if ($bean->id == 0)
        Flight::halt(404, 'Error: invalid user id');

    $username = trim(Flight::request()->query->username);
    $password = Flight::request()->query->password;
    $money = intval(trim(Flight::request()->query->money));

    if (empty($username) || $username == 'admin' || mb_strlen($username) > 200 || empty($password) || mb_strlen($password) > 200)
        Flight::halt(400, 'Error! Invalid username or password');

    $bean->username = $username;
    $bean->password = $password;
    $bean->money    = $money;
    $id = R::store($bean);

    Flight::json(true);
});

define_admin_api('get_player', function() {
    $user_id = Flight::request()->query->user_id;

    $bean = R::load('player', $user_id);
    if ($bean->id == 0)
        Flight::halt(404, 'Error: invalid user id');

    Flight::json([
        'id' => $bean->id,
        'username' => $bean->username,
        'password' => $bean->password,
        'money' => $bean->money
    ]);
});

define_admin_api('destroy_player', function() {
    $user_id = Flight::request()->query->user_id;

    $bean = R::load('player', $user_id);
    if ($bean->id == 0)
        Flight::halt(404, 'Error: invalid user id');

    R::trash($bean);

    Flight::json(true);
});

define_admin_api('begin_round', function() {
    $game_id = Flight::request()->query->game_id;
    $time = Flight::request()->query->time;

    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(404, 'Error: invalid game id');

    $time = $time*60 +  time();

    $bean->status = 1;
    $bean->finished_time = $time;

    R::store($bean);

    Flight::json(true);
});

define_admin_api('stop_round', function() {
    $game_id = Flight::request()->query->game_id;

    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(404, 'Error: invalid game id');

    $bean->status = 1;
    $bean->finished_time = 0;

    R::store($bean);

    Flight::json(true);
});

define_admin_api('finalize_round', function() {
    $game_id = Flight::request()->query->game_id;

    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(404, 'Error: invalid game id');

    $SQL = <<<TAG
          SELECT p.id player_id, a.attempt attempt
          FROM player p
          LEFT JOIN (
            SELECT max(saved_time) _, player_id, attempt
            FROM attempt
            WHERE round = ?
            GROUP BY player_id) a
          ON a.player_id = p.id
          WHERE p.game_id = ?
          ORDER BY p.id ASC
TAG;
    $result = R::getAll($SQL, [$bean->round, $game_id]);

    $DEFAULT_ATTEMPT = json_decode(json_encode([
        "engine"    => [0,0,0,0,0,0],
        "distance"  => $bean->round <= 2 ? 4 : 0,
        "pay_amnt"  => 0,
        "auction"   => [0,0,0,0,0,0],
        "choose"    => -1,
        "pool_amnt" => 0,
    ]));

    $attempts = [];
    foreach ($result as $row) {
        $attempts[] = is_null($row['attempt']) ? $DEFAULT_ATTEMPT : json_decode($row['attempt']);
    }

    $preprocessed = preprocess_attempts($attempts);
    $processed = null;
    if ($bean->round <= 2)
        $processed = process_action_attempt_a($attempts, $preprocessed);
    else
        $processed = process_action_attempt_b($attempts, $preprocessed);

    // FIXME process all pending action here!
    R::transaction(function() use ($bean, $result, $preprocessed, $processed) {

        // Store raw result
        $bresult = R::dispense('result');
        $bresult->game = $bean;
        $bresult->round = $bean->round;
        $bresult->result = json_encode([$preprocessed, $processed]);
        $bresult->time = time();
        R::store($bresult);

        // Change user money
        foreach ($result as $i=>$v) {
            $user_id = $v['player_id'];
            $user_bean = R::load('player', $user_id);
            $user_bean->money += $processed[0][$i];
            R::store($user_bean);
        }

        // Update game status
        $bean->distance += $processed[1]['distance'];
        $bean->hp       += $processed[1]['hp'];
        $bean->food     += $processed[1]['food'];
        $bean->power    += $processed[1]['power'];
        $bean->internet += $processed[1]['internet'];
        $bean->money    += $processed[1]['pool'];

        $bean->status = 0;
        $bean->round = $bean->round+1;
        R::store($bean);
    });

    Flight::json(true);
});


