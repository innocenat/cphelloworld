<?php
require 'flight/Flight.php';
require 'api.php';

$attempts = [
    [
        "engine"    => [500, -500, 500, 1200, -1200, 1200],
        "distance"  => 0,
        "pay_amnt"  => 1200,
        "auction"   => [2, 0, 0, 1, 0, 0],
        "choose"    => 0,
        "pool_amnt" => 0,
    ],
    [
        "engine"    => [500, -500, 500, 1200, -1200, 1200],
        "distance"  => 3,
        "pay_amnt"  => 1200,
        "auction"   => [1, 0, 0, 2, 0, 0],
        "choose"    => 0,
        "pool_amnt" => 0,
    ],
    [
        "engine"    => [500, -500, 500, 1200, -1200, 1200],
        "distance"  => 3,
        "pay_amnt"  => 1200,
        "auction"   => [0, 3, 0, 0, 0, 0],
        "choose"    => 0,
        "pool_amnt" => 0,
    ],
    [
        "engine"    => [500, -500, 500, 1200, -1200, 1200],
        "distance"  => 3,
        "pay_amnt"  => 1201,
        "auction"   => [0, 0, 3, 0, 0, 0],
        "choose"    => 0,
        "pool_amnt" => 0,
    ],
];

$attempts = json_decode(json_encode($attempts));

$preprocessed = preprocess_attempts($attempts);
$result = process_action_attempt_a($attempts, $preprocessed);
echo '<pre>';
var_dump($result);
