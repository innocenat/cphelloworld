<?php

define_user_api('status', function() {
    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    if ($bean->round >= 5) {
        Flight::json(false);
        Flight::stop();
    }

    // Get player
    $player = R::findOne('player', 'username = ?', [$_SESSION['login']]);
    if (!$player)
        Flight::halt(500, 'ERROR: Invalid logged in user!');

    // Transaction
    $trans = [];
    if ($bean->status == 1) {
        $transactions = R::find('transaction', '([from] = ? OR [to] = ?) AND round = ?', [$player->id, $player->id, $bean->round]);
        foreach ($transactions as $transaction) {
            if ($transaction->from == $player->id) {
                $type = "to" ;
                $tuser = R::load('player', $transaction->to)->username;
            } else {
                $type = "from";
                $tuser = R::load('player', $transaction->from)->username;
            }
            $trans[] = ['type' => $type, 'user' => $tuser, 'amount' => $transaction->amnt, 'time' => $transaction->time];
        }
    }

    $info = [
        'game_id' => $bean->id,
        'round' => $bean->round,
        'distance' => $bean->distance,
        'power' => $bean->power,
        'hp' => $bean->hp,
        'food' => $bean->food,
        'internet' => $bean->internet,
        'money' => $bean->money,
        'status' => $bean->status,
        'finished_time' => $bean->finished_time,
        'player_money' => $player->money,

        'transactions' => $trans,
    ];

    Flight::json($info);
});

define_user_api('get_action_detail', function() {
    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    if ($bean->round >= 5) {
        Flight::json(false);
        Flight::stop();
    }

    $round = $bean->round;
    $status = $bean->status;

    if ($status == 0 || time() > $bean->finished_time) {
        Flight::json(false);
        Flight::stop();
    }

    $user_bean = R::findOne('player', 'username = ?', [$_SESSION['login']]);
    if (!$user_bean)
        Flight::halt(500, 'ERROR: Invalid logged in user!');

    $attempt = R::findOne('attempt', 'game_id = ? AND player_id = ? AND round = ? ORDER BY saved_time DESC', [$bean->id, $user_bean->id, $round]);
    if (!$attempt) {
        $attempt_to_send = [
            "engine"    => [0,0,0,0,0,0],
            "distance"  => $bean->round <= 2 ? 4 : 0,
            "pay_amnt"  => 0,
            "auction"   => [0,0,0,0,0,0],
            "choose"    => -1,
            "pool_amnt" => 0,
        ];
    } else {
        $attempt_to_send = json_decode($attempt->attempt);
    }

    // Player list
    $players = R::find('player', 'game_id = ?', [$bean->id]);
    $player_list = [];
    foreach ($players as $player) {
        if ($player->username != $_SESSION['login'])
            $player_list[] = ['username' => $player->username, 'id' => $player->id];
    }


    if ($round <= 2) {
        Flight::json([
            'distance_price' => [500,300,200,100,0],
            'distance_label' => [2,1,0,-1,-2],
            'distance_default' => 4,
            'auction_mode' => 0,
            'pay_allow_negative' => false,
            'choose_price' => 500,
            'current_attempt' => $attempt_to_send,
            'player_list' => $player_list,
        ]);
    } else {
        Flight::json([
            'distance_price' => [0,100,200,300,500],
            'distance_label' => [2,1,0,-1,-2],
            //'distance_label' => [1,0.5,0,-0.5,-1],
            'distance_default' => 0,
            'auction_mode' => 1,
            'pay_allow_negative' => true,
            'choose_price' => 1000,
            'current_attempt' => $attempt_to_send,
            'player_list' => $player_list,
        ]);
    }
});

define_user_api('load_latest_action', function() {
    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    $round = $bean->round;
    $status = $bean->status;
    if ($status == 0 || time() > $bean->finished_time) {
        Flight::json(false);
        Flight::stop();
    }

    $user_bean = R::findOne('player', 'username = ?', [$_SESSION['login']]);
    if (!$user_bean)
        Flight::halt(500, 'ERROR: Invalid logged in user!');

    $attempt = R::findOne('attempt', 'game_id = ? AND player_id = ? AND round = ? ORDER BY saved_time DESC', [$bean->id, $user_bean->id, $round]);
    if (!$attempt) {
        Flight::json([
            "engine"    => [0,0,0,0,0,0],
            "distance"  => $bean->round <= 2 ? 4 : 0,
            "pay_amnt"  => 0,
            "auction"   => [0,0,0,0,0,0],
            "choose"    => -1,
            "pool_amnt" => 0,
        ]);
    } else {
        Flight::json(json_decode($attempt->attempt));
    }
});

define_user_api('transfer_money', function() {
    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    $user_bean = R::findOne('player', 'username = ?', [$_SESSION['login']]);
    if (!$user_bean)
        Flight::halt(500, 'ERROR: Invalid logged in user!');

    $amnt = intval(Flight::request()->query->amount);
    $target = intval(Flight::request()->query->user_id);

    $target_bean = R::load('player', $target);
    if ($target_bean->id == 0 || $target_bean->game_id != $game_id || $user_bean->id == $target)
        Flight::halt(400, 'ERROR: Invalid target user');
    if ($amnt < 0) {
        Flight::halt(400, 'ERROR: Negative money.');
    }

    $t = R::dispense('transaction');
    $t->from = $user_bean->id;
    $t->to = $target;
    $t->time = time();
    $t->amnt = $amnt;
    $t->round = $bean->round;

    $SQL = '
        UPDATE player
        SET money = ( CASE
          WHEN (id = :t) THEN money+:amnt
          ELSE               money-:amnt
        END )
        WHERE id IN(:t,:s)';

    // SQLite Whole Database Lock
    R::exec('BEGIN EXCLUSIVE TRANSACTION');
    try {
        // Check if amount if sufficient
        $user_bean->fresh();
        if ($amnt > $user_bean->money) {
            R::exec('ROLLBACK');
            Flight::halt(400, 'ERROR: ');
        }

        R::exec($SQL, ['t' => $target, 's' => $user_bean->id, 'amnt' => $amnt]);
        R::store($t);

        R::exec('COMMIT');
    } catch (\Exception $e) {
        R::exec('ROLLBACK');
    }

    Flight::json(true);
});

define_user_api('save_action', function() {
    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    $round = $bean->round;
    $status = $bean->status;

    if ($status == 0 || time() > $bean->finished_time) {
        Flight::json(false);
        Flight::stop();
    }

    $user_bean = R::findOne('player', 'username = ?', [$_SESSION['login']]);
    if (!$user_bean)
        Flight::halt(500, 'ERROR: Invalid logged in user!');

    $attempt = Flight::request()->data->attempt;
    $attempt_decoded = json_decode($attempt);

    if (false === validate_action_attempt($user_bean->money, $round, $attempt_decoded)) {
        Flight::json(false);
        Flight::stop();
    }

    integerize_attempt($attempt_decoded);
    $attempt = json_encode($attempt_decoded);

    $attempt_saved = R::dispense('attempt');
    $attempt_saved->player = $user_bean;
    $attempt_saved->round = $round;
    $attempt_saved->game = $bean;
    $attempt_saved->attempt = $attempt;
    $attempt_saved->saved_time = time();

    R::store($attempt_saved);

    Flight::json(true);
});
