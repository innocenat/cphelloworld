<?php
const BASE = '/prog/2015/CPHelloWorld/';
require 'flight/Flight.php';
require 'rb.php';

session_start();
mb_internal_encoding('utf-8');

R::setup('sqlite:db/game.sqlite');

Flight::view()->set('base', BASE);

Flight::route('/', function() {
    Flight::render('login');
});

Flight::route('/game', function() {
    if (empty($_SESSION['login']) || $_SESSION['login'] == 'admin')
        Flight::forbidden();
    Flight::render('main');
});

Flight::route('/admin', function() {
    if (empty($_SESSION['login']) || $_SESSION['login'] != 'admin')
        Flight::forbidden();
    Flight::render('admin');
});

Flight::route('/logout', function() {
    $_SESSION['login'] = '';
    session_write_close();
    Flight::redirect('');
});

Flight::route('POST /login', function() {
    $req = Flight::request();
    $username = $req->data->username;
    $password = $req->data->password;

    $login_error = false;

    if ($username == 'admin') {
        if ($password == 'admin') {
            $_SESSION['login'] = 'admin';
            Flight::redirect('admin');
            Flight::stop();
        }
    } else {
        $bean = R::findOne('player', 'username = ? and game_id = ?', [$username, get_global_setting('current_game', -1)]);
        if ($bean && $bean->password == $password) {
            $_SESSION['login'] = $username;
            Flight::redirect('game');
            Flight::stop();
        }
    }
    Flight::render('login', ['error' => true]);
});

Flight::map('forbidden', function() {
    Flight::halt(403, '<h1>Access forbidden</h1>');
});

// API Endpoint
require 'api.php';

Flight::start();

