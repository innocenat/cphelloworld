<?php

Flight::route('/show', function() {
    Flight::render('show.php');
});
Flight::route('/show/json', function() {
    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

//    if ($bean->round > 4) {
//        Flight::json(false);
//        Flight::stop();
//    }

    $info = [
        'id' => $game_id,
        'round' => $bean->round,
        'distance' => $bean->distance,
        'power' => $bean->power,
        'hp' => $bean->hp,
        'food' => $bean->food,
        'internet' => $bean->internet,
        'money' => $bean->money,
        'status' => $bean->status,
        'finished_time' => $bean->finished_time,
    ];

    Flight::json($info);
});

function define_user_api($path, $closure) {
    Flight::route('/api/game/' . $path, function() use ($closure) {
        if (empty($_SESSION['login']))
            Flight::forbidden();
        $closure();
    });
}
function define_admin_api($path, $closure) {
    Flight::route('/api/admin/' . $path, function() use ($closure) {
        if (empty($_SESSION['login']) && $_SESSION['login'] != 'admin')
            Flight::forbidden();
        $closure();
    });
}

function get_global_setting($name, $default) {
    $setting_bean = R::findOne('setting', 'name = ?', [$name]);
    if ($setting_bean) {
        return $setting_bean->value;
    }
    return $default;
}

function set_global_setting($name, $value) {
    $setting_bean = R::findOne('setting', 'name = ?', [$name]);
    if (!$setting_bean) {
        $setting_bean = R::dispense('setting');
        $setting_bean->name = $name;
    }
    $setting_bean->value = $value;
    R::store($setting_bean);
    return $value;
}

function validate_action_attempt($money, $round, $attempt) {
    $required_money = 0;

    // 1. Engine
    if (count($attempt->engine) != 6)
        return false;
    for ($i = 0; $i < 6; $i++) {
        $required_money += abs($attempt->engine[$i]);
    }

    // 2. Distance
    // 0 = +200, 1 +100, 2 0, 3 -100, 4 -200
    if ($attempt->distance < 0 || $attempt->distance > 4)
        return false;

    if ($round <= 2) {
        $required_money += (4-$attempt->distance)*100 + ($attempt->distance == 0 ? 100 : 0);
    } else {
        $required_money += ($attempt->distance)*100 + ($attempt->distance == 4 ? 100 : 0);
    }

    // 3. Pay it up
    if ($attempt->pay_amnt < 0 && $round <= 2)
        return false;
    $required_money += abs($attempt->pay_amnt);

    // 4. Auction
    if (count($attempt->auction) != 6)
        return false;
    if ($round <= 2) {
        $vote_count = 0;
        for ($i = 0; $i < 6; $i++) {
            $vote_count += $attempt->auction[$i];
            if ($attempt->auction[$i] < 0)
                return false;
        }
        if ($vote_count > 3)
            return false;
        $required_money += $vote_count * 500;
    } else {
        for ($i = 0; $i < 6; $i++) {
            $required_money += abs($attempt->auction[$i]);
        }
    }

    // 5. Choose
    if ($attempt->choose != -1)
        $required_money += 500;
    if ($attempt->choose > 6 || $attempt->choose < -1)
        return false;

    // Central pool
    $required_money += abs($attempt->pool_amnt);

    if ($money < $required_money && $money !== false) // allow using false to skip money checking
        return false;
    return $required_money;
}

function preprocess_attempts($attempts) {
    $processed = [
        'engine'  => [0,0,0,0,0,0],
        'pay_sum' => 0,
        'auction' => [0,0,0,0,0,0],
        'choose'  => [0,0,0,0,0,0,0],
        'pool_amnt' => 0,
    ];

    foreach($attempts as $attempt) {
        for ($i = 0; $i < 6; $i++) {
            $processed['engine'] [$i] += $attempt->engine[$i];
            $processed['auction'][$i] += $attempt->auction[$i];
        }
        if ($attempt->choose != -1)
            $processed['choose'][$attempt->choose]++;
        $processed['pay_sum'] += $attempt->pay_amnt;
        $processed['pool_amnt'] += $attempt->pool_amnt;
    }

    return $processed;
}

function process_action_attempt_a($attempts, $preprocessed) {
    $final_money = [];
    $status = [
        'distance' => 0,
        'hp' => 0,
        'power' => 0,
        'food' => 0,
        'internet' => 0,
        'pool' => $preprocessed['pool_amnt'],
    ];

    $result = [
        'engine' => [],
        'auction' => [],
        'choose' => [],
    ];

    foreach ($attempts as $attempt) {
        $required_money = validate_action_attempt(false, 1, $attempt);
        if ($required_money === false) {
            // FIXME should not happen
        }

        $dMoney = -$required_money + 10000; // Additional 10000 per round!

        $final_money[] = $dMoney;

        // Process 2. Distance choosing
        $status['distance'] += (2-$attempt->distance);
    }

    // Process 1. Engine
    static $_engine_info = [
        [ 0.5,  0.5,    0, -0.5,  5000],
        [   0,  0.5, -0.5,  0.5,  5000],
        [-0.5,    0,  0.5,  0.5,  5000],
        [ 0.5, -0.5,  0.5,    0,  5000],
        [-0.5, -0.5, -0.5, -0.5,  7500],
        [   1,    1,    1,    1, 15000],
    ];

    for ($i = 0; $i < 6; $i++) {
        if ($preprocessed['engine'][$i] >= $_engine_info[$i][4]) {
            $status['food']     += $_engine_info[$i][0];
            $status['hp']       += $_engine_info[$i][1];
            $status['power']    += $_engine_info[$i][2];
            $status['internet'] += $_engine_info[$i][3];

            $result['engine'][] = $i;
        }
    }

    // Process 3. Free money
    $prize = [20000, 18000, 18000, 15000, 15000, 12000, 12000, 12000];
    $t_arr = [];
    foreach ($attempts as $i=>$attempt) {
        if (isset($t_arr[$attempt->pay_amnt]))
            $t_arr[$attempt->pay_amnt][] = $i;
        else
            $t_arr[$attempt->pay_amnt] = [$i];
    }
    krsort($t_arr);
    $i = 0;
    foreach ($t_arr as $v) {
        $sum = 0;
        for ($ii = $i; $ii < $i+count($v); $ii++)
            $sum += $prize[$ii];
        $i += count($v);
        foreach ($v as $k)
            $final_money[$k] += floor($sum / count($v));
    }


    // Process 4. Auction
    static $_auction_info = [
        [   1,   0, -0.5,    0],
        [   0, 0.5,    1, -0.5],
        [   0,   1,    0,    1],
        [-0.5,   0, -0.5,    1],
        [   0, 0.5,    1, -0.5],
        [ 0.5,  -1,    0,  0.5],
    ];
    $_max = PHP_INT_MAX;
    $_max_count = 0;

    foreach ($preprocessed['auction'] as $val) {
        $cnt = 0;
        foreach ($preprocessed['auction'] as $v2) {
            if ($v2 >= $val) $cnt++;
        }
        if ($cnt <= 3 && $cnt >= $_max_count) {
            $_max = $val;
            $_max_count = $cnt;
        }
    }
    for ($i = 0; $i < 6; $i++) {
        if ($preprocessed['auction'][$i] >= $_max && $preprocessed['auction'][$i] > 0) {
            $status['food']     += $_auction_info[$i][0];
            $status['hp']       += $_auction_info[$i][1];
            $status['power']    += $_auction_info[$i][2];
            $status['internet'] += $_auction_info[$i][3];

            $result['auction'][] = $i;
        }
    }

    // Process 5. Choose an Item
    static $_item_info = [
        [-0.5,    0, -0.5, -0.5, 5000, 1],
        [   0,    1,    0, -0.5, 2000, 1],
        [   1, -0.5,    0,    0, 2000, 1],
        [-0.5,    0,    0,    1, 2000, 1],
        [ 0.5,    0,  0.5,    0,    0, 1],
        [   0,    0,  0.5,    1, 4000, 2],
        [ 0.5,  0.5,  0.5,  0.5,    0, 2],
    ];
    // Status
    for ($i = 0; $i < 7; $i++) {
        if ($preprocessed['choose'][$i] == $_item_info[$i][5]) {
            $status['food']     += $_item_info[$i][0];
            $status['hp']       += $_item_info[$i][1];
            $status['power']    += $_item_info[$i][2];
            $status['internet'] += $_item_info[$i][3];

            $result['choose'][] = $i;
        }
    }
    // Money
    for ($i = 0; $i < count($attempts); $i++) {
        if (in_array($attempts[$i]->choose, $result['choose'])) {
            $final_money[$i] += $_item_info[$attempts[$i]->choose][4];
        }
    }

    return [$final_money, $status, $result];
}

function process_action_attempt_b($attempts, $preprocessed) {
    $final_money = [];
    $status = [
        'distance' => 0,
        'hp' => 0,
        'power' => 0,
        'food' => 0,
        'internet' => 0,
        'pool' => $preprocessed['pool_amnt'],
    ];

    $result = [
        'engine' => [],
        'auction' => [],
        'choose' => [],
    ];

    foreach ($attempts as $attempt) {
        $required_money = validate_action_attempt(false, 1, $attempt);
        if ($required_money === false) {
            // FIXME should not happen
        }

        $dMoney = -$required_money + 10000; // Additional 10000 per round!

        $final_money[] = $dMoney;

        // Process 2. Distance choosing
        $status['distance'] += (2-$attempt->distance) ;// /2;
    }

    // Process 1. Engine
    static $_engine_info = [
        [   1,    0,    0,  0,  7000],
        [   0,    1,    0,  0,  7000],
        [   0,    0,    1,  0,  7000],
        [   0,    0,    0,  1,  7000],
        [  -1,   -1,   -1, -1, 10000],
        [-0.5, -0.5, -0.5,  1, 10000],
    ];

    for ($i = 0; $i < 6; $i++) {
        if ($preprocessed['engine'][$i] >= $_engine_info[$i][4]) {
            $status['food']     += $_engine_info[$i][0];
            $status['hp']       += $_engine_info[$i][1];
            $status['power']    += $_engine_info[$i][2];
            $status['internet'] += $_engine_info[$i][3];

            $result['engine'][] = $i;
        }
    }

    // Process 3. Help money
    if ($preprocessed['pay_sum'] < 20000) {
        $status['food']     -= 1;
        $status['hp']       -= 1;
        $status['power']    -= 1;
        $status['internet'] -= 1;
        //$status['distance'] -= 1;
        //$status['pool']     -= 2*(20000 - $preprocessed['pay_sum']);
    } else {
        $status['food']     += 1;
        $status['hp']       += 1;
        $status['power']    += 1;
        $status['internet'] += 1;
        //$status['distance'] += 1;
    }


    // Process 4. Auction
    static $_auction_info = [
        [ 0.5,   0, -0.5,  0.5],
        [-0.5,   1,    1, -0.5],
        [   0, 0.5,    1,    0],
        [ 0.5,  -1,    0,  0.5],
        [-0.5,   0, -0.5,  0.5],
        [   1, 0.5,    0,    1],
    ];
    $_max = PHP_INT_MAX;
    $_max_count = 0;

    foreach ($preprocessed['auction'] as $val) {
        $cnt = 0;
        foreach ($preprocessed['auction'] as $v2) {
            if ($v2 >= $val) $cnt++;
        }
        if ($cnt <= 3 && $cnt >= $_max_count) {
            $_max = $val;
            $_max_count = $cnt;
        }
    }
    for ($i = 0; $i < 6; $i++) {
        if ($preprocessed['auction'][$i] >= $_max && $preprocessed['auction'][$i] > 0) {
            $status['food']     += $_auction_info[$i][0];
            $status['hp']       += $_auction_info[$i][1];
            $status['power']    += $_auction_info[$i][2];
            $status['internet'] += $_auction_info[$i][3];

            $result['auction'][] = $i;
        }
    }

    // Process 5. Choose an Item
    static $_item_info = [
        [   0,  0.5,    0,    1,    0, 1],
        [-0.5,    0,    0, -0.5, 3000, 1],
        [ 0.5, -0.5,    1,    0,    0, 1],
        [ 0.5,    0, -0.5,    0, 2000, 1],
        [-0.5, -0.5,    0, -0.5, 5000, 2],
        [   0,  0.5,  0.5,  0.5,    0, 2],
        [   1,    1,    0,    1,    0, 3],
    ];
    // Status
    for ($i = 0; $i < 7; $i++) {
        if ($preprocessed['choose'][$i] == $_item_info[$i][5]) {
            $status['food']     += $_item_info[$i][0];
            $status['hp']       += $_item_info[$i][1];
            $status['power']    += $_item_info[$i][2];
            $status['internet'] += $_item_info[$i][3];

            $result['choose'][] = $i;
        }
    }
    // Money
    for ($i = 0; $i < count($attempts); $i++) {
        if (in_array($attempts[$i]->choose, $result['choose'])) {
            $final_money[$i] += $_item_info[$attempts[$i]->choose][4];
        }
    }

    return [$final_money, $status, $result];
}

function integerize_attempt(&$attempt) {
    // 1. Engine
    for ($i = 0; $i < 6; $i++) {
        $attempt->engine[$i] = floor($attempt->engine[$i]);
    }

    // 3. Pay it up
    $attempt->pay_amnt = floor($attempt->pay_amnt);

    // 4. Auction
    for ($i = 0; $i < 6; $i++) {
        $attempt->auction[$i] = floor($attempt->auction[$i]);
    }
}

require 'api_user.php';
require 'api_adm.php';

if (isset($_SESSION['login'])) {
    Flight::view()->set('login', $_SESSION['login']);
}

Flight::route('/admin/result', function() {
    if (empty($_SESSION['login']) || $_SESSION['login'] != "admin") {
        Flight::forbidden();
    }

    $game_id = get_global_setting('current_game', -1);
    $bean = R::load('game', $game_id);
    if ($bean->id == 0)
        Flight::halt(400, 'ERROR: Invalid game id');

    $round = $bean->round;

    $round--;

    if (isset($_GET['round']))
        $round = intval($_GET['round']);

    // FIND USER POOL MONEY
    $SQL = <<<TAG
          SELECT p.id player_id, a.attempt attempt
          FROM player p
          LEFT JOIN (
            SELECT max(saved_time) _, player_id, attempt
            FROM attempt
            WHERE round = ?
            GROUP BY player_id) a
          ON a.player_id = p.id
          WHERE p.game_id = ?
          ORDER BY p.id ASC
TAG;
    $result = R::getAll($SQL, [$round, $game_id]);
    $DEFAULT_ATTEMPT = json_decode(json_encode([
        "engine"    => [0,0,0,0,0,0],
        "distance"  => $bean->round <= 2 ? 4 : 0,
        "pay_amnt"  => 0,
        "auction"   => [0,0,0,0,0,0],
        "choose"    => -1,
        "pool_amnt" => 0,
    ]));

    $attempts = [];
    foreach ($result as $row) {
        $attempts[] = is_null($row['attempt']) ? $DEFAULT_ATTEMPT : json_decode($row['attempt']);
    }

    $money = [];
    foreach ($attempts as $attempt) {
        $money[] = intval($attempt->pay_amnt);
    }
    rsort($money);

    $result = R::findOne('result', 'round = ? AND game_id = ? ORDER BY time DESC', [$round, $bean->id]);
    if (!$result) {
        echo "<h1>No result yet</h1>";
        Flight::stop();
    }

    Flight::view()->set('result', json_decode($result->result));
    Flight::view()->set('round', $round);
    Flight::view()->set('money', $money);

    Flight::render('result');
});
