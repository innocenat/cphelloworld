<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Let's go to Pluto! - Hello CP World</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <link rel="stylesheet" href="<?=$base?>cp.css">
    <script src="<?=$base?>js/view.helpers.js?v1"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Let's go to Pluto!</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" id="alert-prepend">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Game summary</h3>
                </div>
                <div class="panel-body" id="summary-box">
                    <div class="row">
                        <div class="col-xs-2 cp-text-large text-right">Welcome, {{login}}</div>
                        <div class="col-xs-2 cp-text-large text-right">Current round</div>
                        <div class="col-xs-2 cp-text-large" id="current-round-lbl"></div>
                        <div class="col-xs-2 cp-text-large text-right">Time left</div>
                        <div class="col-xs-4 cp-text-large" id="current-round-lbl">Pending</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Current status</h3>
                </div>
                <table class="table" id="game-status-table">
                    <tr><th>Distance</th><td>0</td></tr>
                    <tr><th>HP</th><td>0</td></tr>
                    <tr><th>Power</th><td>0</td></tr>
                    <tr><th>Food</th><td>0</td></tr>
                    <tr><th>Internet</th><td>0</td></tr>
                    <tr><th>Money pool</th><td>0</td></tr>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary" id="panel-user-status">
                <div class="panel-heading">
                    <h3 class="panel-title">Current action</h3>
                </div>
                <table class="table" id="user-status-table">
                    <tr><th>Current money</th><td></td></tr>
                    <tr><th>Spent this round</th><td></td></tr>
                    <tr><th>Provisional money</th><td></td></tr>
                </table>
                <div class="panel-body" id="action-save-box" style="display:none">
                    <button class="btn btn-success" id="save-btn">Save current action</button> <span id="saving-text" class="warning-text">Your action haven't been saved.</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="action-panel">
        <div class="col-md-12" id="action-panel-inner">

        </div>
    </div>

</div>

<script id="template-alert" type="text/x-handlebars-template">
    <div class="alert alert-{{type}} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{{text}}}
    </div>
</script>
<script id="template-summary-box" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-xs-2 cp-text-large text-right">User: <?=$login?></div>
        <div class="col-xs-3 cp-text-large text-right">Current round</div>
        <div class="col-xs-2 cp-text-large" id="current-round-lbl">#{{current_round}}</div>
        <div class="col-xs-2 cp-text-large text-right">Time left</div>
        <div class="col-xs-2 cp-text-large" id="current-round-lbl">{{#if status}}{{time_text finished_time}}{{else}}Pending{{/if}}</div>
    </div>
</script>
<script id="template-game-status-box" type="text/x-handlebars-template">
    <tbody>
        <tr><th>Distance</th><td>{{distance}}</td></tr>
        <tr><th>Food</th><td>{{food}}</td></tr>
        <tr><th>HP</th><td>{{hp}}</td></tr>
        <tr><th>Energy</th><td>{{power}}</td></tr>
        <tr><th>Internet</th><td>{{internet}}</td></tr>
        <tr><th>Money pool</th><td>{{money}}</td></tr>
    </tbody>
</script>
<script id="template-player-status-box" type="text/x-handlebars-template">
    <tbody>
        <tr><th>Current money</th><td>{{money}}</td></tr>
        <tr><th>Spent this round</th><td>{{spent}}</td></tr>
        <tr><th>Leftover</th><td>{{#if_overspent}}<span style="color:red">{{/if_overspent}}{{calc_left_money}}{{#if_overspent}}</span>{{/if_overspent}}</td></tr>
    </tbody>
</script>
<script id="template-game-action-box" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">(#1) Engine</h3>
                </div>
                <div class="panel-body">
                    You can pay for each item, or you can choose to reduce money.
                </div>
                <table class="table">
                    <tr>
                        <th><label for="eng1_value">Engine #1</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.engine.[0]}} active{{/if_pos}}">
                                    <input type="radio" name="eng1_sign" id="eng1_signp" autocomplete="off" value="1"{{#if_pos current_attempt.engine.[0]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.engine.[0]}} active{{/if_neg}}">
                                    <input type="radio" name="eng1_sign" id="eng1_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.engine.[0]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.engine.[0]}}" required id="eng1_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="eng2_value">Engine #2</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.engine.[1]}} active{{/if_pos}}">
                                    <input type="radio" name="eng2_sign" id="eng2_signp" autocomplete="off" value="1"{{#if_pos current_attempt.engine.[1]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.engine.[1]}} active{{/if_neg}}">
                                    <input type="radio" name="eng2_sign" id="eng2_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.engine.[1]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.engine.[1]}}" required id="eng2_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="eng3_value">Engine #3</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.engine.[2]}} active{{/if_pos}}">
                                    <input type="radio" name="eng3_sign" id="eng3_signp" autocomplete="off" value="1"{{#if_pos current_attempt.engine.[2]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.engine.[2]}} active{{/if_neg}}">
                                    <input type="radio" name="eng3_sign" id="eng3_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.engine.[2]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.engine.[2]}}" required id="eng3_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="eng4_value">Engine #4</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.engine.[3]}} active{{/if_pos}}">
                                    <input type="radio" name="eng4_sign" id="eng4_signp" autocomplete="off" value="1"{{#if_pos current_attempt.engine.[3]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.engine.[3]}} active{{/if_neg}}">
                                    <input type="radio" name="eng4_sign" id="eng4_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.engine.[3]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.engine.[3]}}" required id="eng4_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="eng5_value">Engine #5</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.engine.[4]}} active{{/if_pos}}">
                                    <input type="radio" name="eng5_sign" id="eng5_signp" autocomplete="off" value="1"{{#if_pos current_attempt.engine.[4]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.engine.[4]}} active{{/if_neg}}">
                                    <input type="radio" name="eng5_sign" id="eng5_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.engine.[4]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.engine.[4]}}" required id="eng5_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="eng6_value">Engine #6</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.engine.[5]}} active{{/if_pos}}">
                                    <input type="radio" name="eng6_sign" id="eng6_signp" autocomplete="off" value="1"{{#if_pos current_attempt.engine.[5]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.engine.[5]}} active{{/if_neg}}">
                                    <input type="radio" name="eng6_sign" id="eng6_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.engine.[5]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.engine.[5]}}" required id="eng6_value">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">(#2) Distance</h3>
                </div>
                <div class="panel-body">
                    <p>Select distance to buy. You must choose one.</p>
                    <div class="radio">
                        <label>
                            <input type="radio" name="radio-distance" id="radio-distance-0" value="0"{{#if_eq current_attempt.distance 0}} checked{{/if_eq}}>
                            Distance {{format_number action_detail.distance_label.[0]}}
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="radio-distance" id="radio-distance-1" value="1"{{#if_eq current_attempt.distance 1}} checked{{/if_eq}}>
                            Distance {{format_number action_detail.distance_label.[1]}}
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="radio-distance" id="radio-distance-2" value="2"{{#if_eq current_attempt.distance 2}} checked{{/if_eq}}>
                            Distance {{format_number action_detail.distance_label.[2]}}
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="radio-distance" id="radio-distance-3" value="3"{{#if_eq current_attempt.distance 3}} checked{{/if_eq}}>
                            Distance {{format_number action_detail.distance_label.[3]}}
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="radio-distance" id="radio-distance-4" value="4"{{#if_eq current_attempt.distance 4}} checked{{/if_eq}}>
                            Distance {{format_number action_detail.distance_label.[4]}}
                        </label>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">(#3) Pay it up</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="amnt-paying" class="col-sm-3 control-label">Paying</label>
                            <div class="col-sm-9">
                                <div class="btn-group" id="paying-negative" data-toggle="buttons"{{#unless action_detail.pay_allow_negative}} style="display:none"{{/unless}}>
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.pay_amnt}} active{{/if_pos}}">
                                    <input type="radio" name="pay_sign" id="pay_signp" autocomplete="off" value="1"{{#if_pos current_attempt.pay_amnt}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.pay_amnt}} active{{/if_neg}}">
                                    <input type="radio" name="pay_sign" id="pay_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.pay_amnt}} checked{{/if_neg}}> -
                                </label>
                                </div>
                                <input type="number" step="1" class="form-control control-with-sign" id="amnt-paying" value="{{abs current_attempt.pay_amnt}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default" id="panel-auction">
                <div class="panel-heading">
                    <h3 class="panel-title">(#4) Auction</h3>
                </div>
                {{#if action_detail.auction_mode}}
                <div class="panel-body">
                    You can auction in for each item, or you can choose to reduce money.
                </div>
                <table class="table">
                    <tr>
                        <th><label for="auction1_value">Item #1</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.auction.[0]}} active{{/if_pos}}">
                                    <input type="radio" name="auction1_sign" id="auction1_signp" autocomplete="off" value="1"{{#if_pos current_attempt.auction.[0]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.auction.[0]}} active{{/if_neg}}">
                                    <input type="radio" name="auction1_sign" id="auction1_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.auction.[0]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[0]}}" required id="auction1_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction2_value">Item #2</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.auction.[1]}} active{{/if_pos}}">
                                    <input type="radio" name="auction2_sign" id="auction2_signp" autocomplete="off" value="1"{{#if_pos current_attempt.auction.[1]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.auction.[1]}} active{{/if_neg}}">
                                    <input type="radio" name="auction2_sign" id="auction2_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.auction.[1]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[1]}}" required id="auction2_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction3_value">Item #3</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.auction.[2]}} active{{/if_pos}}">
                                    <input type="radio" name="auction3_sign" id="auction3_signp" autocomplete="off" value="1"{{#if_pos current_attempt.auction.[2]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.auction.[2]}} active{{/if_neg}}">
                                    <input type="radio" name="auction3_sign" id="auction3_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.auction.[2]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[2]}}" required id="auction3_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction4_value">Item #4</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.auction.[3]}} active{{/if_pos}}">
                                    <input type="radio" name="auction4_sign" id="auction4_signp" autocomplete="off" value="1"{{#if_pos current_attempt.auction.[3]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.auction.[3]}} active{{/if_neg}}">
                                    <input type="radio" name="auction4_sign" id="auction4_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.auction.[3]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[3]}}" required id="auction4_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction5_value">Item #5</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.auction.[4]}} active{{/if_pos}}">
                                    <input type="radio" name="auction5_sign" id="auction5_signp" autocomplete="off" value="1"{{#if_pos current_attempt.auction.[4]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.auction.[4]}} active{{/if_neg}}">
                                    <input type="radio" name="auction5_sign" id="auction5_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.auction.[4]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[4]}}" required id="auction5_value">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction6_value">Item #6</label></th>
                        <td>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-xs btn-success{{#if_pos current_attempt.auction.[5]}} active{{/if_pos}}">
                                    <input type="radio" name="auction6_sign" id="auction6_signp" autocomplete="off" value="1"{{#if_pos current_attempt.auction.[5]}} checked{{/if_pos}}> +
                                </label>
                                <label class="btn btn-xs btn-danger{{#if_neg current_attempt.auction.[5]}} active{{/if_neg}}">
                                    <input type="radio" name="auction6_sign" id="auction6_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.auction.[5]}} checked{{/if_neg}}> -
                                </label>
                            </div>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[5]}}" required id="auction6_value">
                        </td>
                    </tr>
                </table>
                {{else}}
                <div class="panel-body">
                    You are allow up to three (3) votes.
                </div>
                <table class="table">
                    <tr>
                        <th><label for="auction1_vote">Item #1</label></th>
                        <td>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[0]}}" required id="auction1_vote">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction2_vote">Item #2</label></th>
                        <td>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[1]}}" required id="auction2_vote">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction3_vote">Item #3</label></th>
                        <td>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[2]}}" required id="auction3_vote">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction4_vote">Item #4</label></th>
                        <td>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[3]}}" required id="auction4_vote">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction5_vote">Item #5</label></th>
                        <td>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[4]}}" required id="auction5_vote">
                        </td>
                    </tr>
                    <tr>
                        <th><label for="auction6_vote">Item #6</label></th>
                        <td>
                            <input type="number" step="1" value="{{abs current_attempt.auction.[5]}}" required id="auction6_vote">
                        </td>
                    </tr>
                </table>
                {{/if}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">(#5) Choose</h3>
                </div>
                <div class="panel-body">
                    <p>Select one item.</p>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio-item-choosing" id="radio-item-choosing-z" value="-1"{{#if_eq current_attempt.choose -1}} checked{{/if_eq}}>
                                    Nothing
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio-item-choosing" id="radio-item-choosing-0" value="0"{{#if_eq current_attempt.choose 0}} checked{{/if_eq}}>
                                    Item #1
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio-item-choosing" id="radio-item-choosing-1" value="1"{{#if_eq current_attempt.choose 1}} checked{{/if_eq}}>
                                    Item #2
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio-item-choosing" id="radio-item-choosing-2" value="2"{{#if_eq current_attempt.choose 2}} checked{{/if_eq}}>
                                    Item #3
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio-item-choosing" id="radio-item-choosing-3" value="3"{{#if_eq current_attempt.choose 3}} checked{{/if_eq}}>
                                    Item #4
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio-item-choosing" id="radio-item-choosing-4" value="4"{{#if_eq current_attempt.choose 4}} checked{{/if_eq}}>
                                    Item #5
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio-item-choosing" id="radio-item-choosing-5" value="5"{{#if_eq current_attempt.choose 5}} checked{{/if_eq}}>
                                    Item #6
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="radio-item-choosing" id="radio-item-choosing-6" value="6"{{#if_eq current_attempt.choose 6}} checked{{/if_eq}}>
                                    Item #7
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Donate to central pool</h3>
                </div>
                <div class="panel-body">
                    <p>
                        Pay to donate to central pool.
                    </p>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="pool-amnt" class="col-sm-3 control-label">Donation</label>
                            <div class="col-sm-9">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-xs btn-success{{#if_pos current_attempt.pool_amnt}} active{{/if_pos}}">
                                        <input type="radio" name="pool_sign" id="pool_signp" autocomplete="off" value="1"{{#if_pos current_attempt.pool_amnt}} checked{{/if_pos}}> +
                                    </label>
                                    <label class="btn btn-xs btn-danger{{#if_neg current_attempt.pool_amnt}} active{{/if_neg}}">
                                        <input type="radio" name="pool_sign" id="pool_signm" autocomplete="off" value="-1"{{#if_neg current_attempt.pool_amnt}} checked{{/if_neg}}> -
                                    </label>
                                </div>
                                <input type="number" step="1" class="form-control control-with-sign" id="pool-amnt" value="{{abs current_attempt.pool_amnt}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Bank transfer</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="transfer-amnt" class="col-sm-3 control-label">Amount</label>
                            <div class="col-sm-9"><input type="number" class="form-control" id="transfer-amnt" value="0" name="amount"></div>
                        </div>
                        <div class="form-group">
                            <label for="transfer-target" class="col-sm-3 control-label">To</label>
                            <div class="col-sm-9">
                                <select id="transfer-target" class="form-control">
                                    <option value="-1">Choose</option>
                                    {{#each player_list}}
                                    <option value="{{id}}">{{username}}</option>
                                    {{/each}}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3"><button type="button" id="btn-transfer" class="btn btn-primary">Transfer</button></div>
                        </div>
                    </form>
                </div>
                <table class="table" id="transaction-history">

                </table>
            </div>
        </div>
    </div>
</script>
<script id="template-transaction-history" type="text/x-handlebars-template">
    <thead>
        <tr><th>Type</th><th>Amount</th><th>Time</th></tr>
    </thead>
    <tbody>
        {{#each transactions_history}}
        <tr{{#if_eq type "to"}} class="warning"{{/if_eq}}{{#if_eq type "from"}} class="success"{{/if_eq}}><td>{{type}}: {{user}}</td><td>{{amount}}</td><td>{{humanize_time time}}</td></tr>
        {{/each}}
    </tbody>
</script>

<script>
    var CPGame = {
        game_id: 0,
        current_round: 0,
        status: 0,
        finished_time: 0,
        action_available: false,
        action_shown: false,

        action_loaded: -1, // Round number for action loaded
        action_loading: false,
        action_detail: {
            distance_price: [0,0,0,0,0],
            distance_label: [2,1,0,-1,-2],
            distance_default: 0,
            auction_mode: 0,
            pay_allow_negative: false,
            choose_price: 500
        },

        current_attempt: {},

        money: {
            money: 0,
            spent: 0
        },

        saved_spent: 0,

        player_list: [],
        transactions_history: []
    };

    var _tmpl_summary_box = Handlebars.compile($('#template-summary-box').html());
    var _tmpl_game_status_box = Handlebars.compile($('#template-game-status-box').html());
    var _tmpl_game_action_box = Handlebars.compile($('#template-game-action-box').html());
    var _tmpl_user_status_box = Handlebars.compile($('#template-player-status-box').html());
    var _tmpl_alert = Handlebars.compile($('#template-alert').html());
    var _tmpl_transaction_history = Handlebars.compile($('#template-transaction-history').html());

    function create_alert(type, text) {
        return _tmpl_alert({'type': type, 'text': text});
    }

    function loadAndShowGameAction() {
        CPGame.action_loading = true;
        $.getJSON('<?=$base?>api/game/get_action_detail', function(res) {
            CPGame.action_loaded = CPGame.current_round;
            CPGame.action_loading = false;
            CPGame.action_detail = res;

            CPGame.current_attempt = res.current_attempt;
            CPGame.player_list = res.player_list;

            showGameAction();
        });
    }

    function showGameAction() {
        $('#action-panel-inner').html(_tmpl_game_action_box(CPGame));
        processAttempt();
        $('#action-save-box').hide();
        CPGame.action_shown = true;
        attachEventHandler();
    }

    function hideGameAction() {
        $('#action-panel-inner').html('');
        CPGame.action_shown = false;
        $('#action-save-box').hide();
        CPGame.money.spent = CPGame.saved_spent;
    }

    function getStatus() {
        $.getJSON('<?=$base?>api/game/status', function(res) {
            if (!res)
                window.location = "<?=$base?>logout";
            CPGame.game_id = res.id;
            CPGame.current_round = res.round;
            CPGame.status = res.status;
            CPGame.finished_time = res.finished_time;

            CPGame.action_available = (res.status != 0);

            CPGame.money.money = +res.player_money;
            CPGame.transactions_history = res.transactions;

            // Clear spent
            if (CPGame.current_round != CPGame.action_loaded) {
                CPGame.money.spent = 0;
                CPGame.money.saved_spent = 0;
            }

            $('#game-status-table').html(_tmpl_game_status_box(res));
            updateSummary();

            // Update action
            if ((!CPGame.action_available || isFinished(CPGame.finished_time)) && CPGame.action_shown) {
                //$('#action-panel-inner').html(_tmpl_game_action_box(CPGame.action_detail));
                hideGameAction();
            } else if (!CPGame.action_shown && CPGame.action_available && !isFinished(CPGame.finished_time)) {
                if (CPGame.action_loaded == CPGame.current_round)
                    showGameAction();
                else if (!CPGame.action_loading)
                    loadAndShowGameAction();
            }
        }).always(function() {
            setTimeout(getStatus, 1000);
        });
    }

    function updateSummary() {
        $('#summary-box').html(_tmpl_summary_box(CPGame));
        $('#user-status-table').html(_tmpl_user_status_box(CPGame.money));

        if (CPGame.action_shown) {
            $('#transaction-history').html(_tmpl_transaction_history(CPGame));
        }
    }

    setInterval(updateSummary, 1000);

    function attachEventHandler() {
        $('#action-panel input[type=number]').blur(processAttempt).keyup(processAttempt).keypress(function(e) {
            var chr = String.fromCharCode(e.which);
            if (chr == '.')
                e.preventDefault();
        }).each(function() {
            if ($(this).parent().find('input[type=radio]:checked').val() > 0)
                $(this).css('background-color', '#cfc');
            else if ($(this).parent().find('input[type=radio]:checked').val() < 0)
                $(this).css('background-color', '#fcc');
        });
        $('#action-panel input').click(processAttempt);
        $('#action-panel .btn-group label').click(processAttempt);

        $('#btn-transfer').click(doTransferMoney);

        $('#action-panel .btn-success').click(function() {
            var pp = $(this).parent().parent();
            pp.find('input').css('background-color', '#cfc');
        });
        $('#action-panel .btn-danger').click(function() {
            var pp = $(this).parent().parent();
            pp.find('input').css('background-color', '#fcc');
        });
    }

    function doTransferMoney() {
        var amnt = $('#transfer-amnt').val();
        var target = $('#transfer-target').val();

        if (target == -1) {
            alert('Please choose a recipient!');
            return;
        }

        if (amnt > CPGame.money.money - CPGame.money.spent) {
            alert('Error! Not enough money!');
            return;
        }

        if (amnt <= 0) {
            alert('Error! Negative or zero money!');
            return;
        }

        $('#transfer-target').prop('disabled', true);
        $.getJSON('<?=$base?>api/game/transfer_money', {amount: amnt, user_id: target})
            .done(function() {
                $('#transfer-amnt').val('0');
            })
            .always(function() {
                $('#transfer-target').prop('disabled', false);
            })
            .fail(function() {
                alert('Something happen during transfer!');
            })
        ;
    }

    function processAttempt() {
        var attempt = summariseAttempt();
        var usage = calculateMoneyUsage(attempt);

        if (!usage.vote_valid) {
            $('#save-btn').prop('disabled', true);
            $('#panel-auction').addClass('panel-danger');
        } else {
            $('#panel-auction').removeClass('panel-danger');
        }

        if (usage.usage > CPGame.money.money) {
            $('#save-btn').prop('disabled', true);
            $('#panel-user-status').addClass('panel-danger');
        } else {
            $('#panel-user-status').removeClass('panel-danger');
        }

        if (usage.vote_valid && usage.usage <= CPGame.money.money) {
            $('#save-btn').prop('disabled', false);
        }

        CPGame.money.spent = usage.usage;
        updateSummary();

        CPGame.current_attempt = attempt;

        $('#action-save-box').show();

        return attempt;
    }

    function cleanupAttempt() {
        // Clean up all number field
        $('#action-panel input[type=number]').val('0');

        // Engine radio
        $('#eng1_signp').click();
        $('#eng2_signp').click();
        $('#eng3_signp').click();
        $('#eng4_signp').click();
        $('#eng5_signp').click();
        $('#eng6_signp').click();

        // Auction radio
        $('#auction1_signp').click();
        $('#auction2_signp').click();
        $('#auction3_signp').click();
        $('#auction4_signp').click();
        $('#auction5_signp').click();
        $('#auction6_signp').click();

        // Item choosing
        $('#radio-item-choosing-z').click();

        // Distance
        $('#radio-distance-' + CPGame.action_detail.distance_default).click();
    }

    function summariseAttempt() {
        var attempt = {
            "engine": [0,0,0,0,0,0],
            "distance": -1,
            "pay_amnt": 0,
            "auction": [0,0,0,0,0,0],
            "choose": -1,
            "pool_amnt": 0
        };

        // Simple cleanup
        $('#action-panel input[type=number]').each(function() {
            var s = $(this);
            if (+s.val() < 0)
                s.val('0');
        });

        // #1
        attempt["engine"][0] = +$('#eng1_value').val() * +$('input[name=eng1_sign]:checked').val();
        attempt["engine"][1] = +$('#eng2_value').val() * +$('input[name=eng2_sign]:checked').val();
        attempt["engine"][2] = +$('#eng3_value').val() * +$('input[name=eng3_sign]:checked').val();
        attempt["engine"][3] = +$('#eng4_value').val() * +$('input[name=eng4_sign]:checked').val();
        attempt["engine"][4] = +$('#eng5_value').val() * +$('input[name=eng5_sign]:checked').val();
        attempt["engine"][5] = +$('#eng6_value').val() * +$('input[name=eng6_sign]:checked').val();

        // #2
        attempt["distance"] = +$('input[name=radio-distance]:checked').val();

        // #3
        attempt["pay_amnt"] = +$('#amnt-paying').val() * +$('input[name=pay_sign]:checked').val();

        // #4
        if (CPGame.action_detail.auction_mode == 0) {
            attempt["auction"][0] = +$('#auction1_vote').val();
            attempt["auction"][1] = +$('#auction2_vote').val();
            attempt["auction"][2] = +$('#auction3_vote').val();
            attempt["auction"][3] = +$('#auction4_vote').val();
            attempt["auction"][4] = +$('#auction5_vote').val();
            attempt["auction"][5] = +$('#auction6_vote').val();
        } else {
            attempt["auction"][0] = +$('#auction1_value').val() * +$('input[name=auction1_sign]:checked').val();
            attempt["auction"][1] = +$('#auction2_value').val() * +$('input[name=auction2_sign]:checked').val();
            attempt["auction"][2] = +$('#auction3_value').val() * +$('input[name=auction3_sign]:checked').val();
            attempt["auction"][3] = +$('#auction4_value').val() * +$('input[name=auction4_sign]:checked').val();
            attempt["auction"][4] = +$('#auction5_value').val() * +$('input[name=auction5_sign]:checked').val();
            attempt["auction"][5] = +$('#auction6_value').val() * +$('input[name=auction6_sign]:checked').val();
        }

        // #5
        attempt["choose"] = +$('input[name=radio-item-choosing]:checked').val();

        // #6
        attempt["pool_amnt"] = +$('#pool-amnt').val() * +$('input[name=pool_sign]:checked').val();

        return attempt;
    }

    function calculateMoneyUsage(attempt) {
        var ret = {
            usage: 0,
            vote_valid: true
        };

        // #1
        ret.usage += Math.abs(attempt.engine[0]);
        ret.usage += Math.abs(attempt.engine[1]);
        ret.usage += Math.abs(attempt.engine[2]);
        ret.usage += Math.abs(attempt.engine[3]);
        ret.usage += Math.abs(attempt.engine[4]);
        ret.usage += Math.abs(attempt.engine[5]);

        // #2
        ret.usage += CPGame.action_detail.distance_price[attempt.distance];

        // #3
        ret.usage += Math.abs(attempt.pay_amnt);

        // #4
        if (CPGame.action_detail.auction_mode == 0) {
            var vote_cnt = 0;
            vote_cnt += attempt.auction[0];
            vote_cnt += attempt.auction[1];
            vote_cnt += attempt.auction[2];
            vote_cnt += attempt.auction[3];
            vote_cnt += attempt.auction[4];
            vote_cnt += attempt.auction[5];

            if (vote_cnt > 3) ret.vote_valid = false;
            ret.usage += 500 * vote_cnt;
        } else {
            ret.usage += Math.abs(attempt.auction[0]);
            ret.usage += Math.abs(attempt.auction[1]);
            ret.usage += Math.abs(attempt.auction[2]);
            ret.usage += Math.abs(attempt.auction[3]);
            ret.usage += Math.abs(attempt.auction[4]);
            ret.usage += Math.abs(attempt.auction[5]);
        }

        // #5
        if (attempt.choose != -1) {
            ret.usage += CPGame.action_detail.choose_price;
        }

        // Pool
        ret.usage += Math.abs(attempt.pool_amnt);

        return ret;
    }

    $(function() {
        getStatus();

        $('#save-btn').click(function() {
            var attempt = processAttempt();
            var att_json = JSON.stringify(attempt);
            $.ajax({
                dataType: "json",
                method: 'post',
                url:'<?=$base?>api/game/save_action',
                data: {attempt:att_json}
            }).fail(function() {
                $(create_alert('danger', '<strong>Error!</strong> Server reject our attempt!')).prependTo($('#alert-prepend'));
            }).done(function(res) {
                if (res) {
                    CPGame.saved_spent = CPGame.money.spent;
                    $('#action-save-box').hide();
                } else {
                    $(create_alert('danger', '<strong>Error!</strong> Attempt is not invalid!')).prependTo($('#alert-prepend'));
                }
            });
        });
    });
</script>
</body>
</html>
