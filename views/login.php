<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login - Hello CP World</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <?php if (!empty($error)): ?>
            <div class="alert alert-danger" role="alert">
                <strong>Error!</strong> Invalid username or password, or your game is not active.
            </div>
        <?php endif ?>
        <div class="panel panel-primary" style="margin-top: 30%">
            <div class="panel-heading">
                <h1 class="panel-title">Login</h1>
            </div>
            <div class="panel-body">
                <form action="<?=$base?>login" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label for="username" class="col-sm-3 control-label">Username</label>
                        <div class="col-sm-9"><input type="text" class="form-control" id="username" placeholder="" name="username"></div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9"><input type="password" class="form-control" id="password" name="password"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3"><button type="submit" class="btn btn-default">Login</button></div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
