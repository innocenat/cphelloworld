<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Let's go to Pluto! - Hello CP World</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <link rel="stylesheet" href="<?=$base?>cp.css">
    <script src="<?=$base?>js/view.helpers.js?v1"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h1>Administration</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Game action</h3>
                </div>
                <div class="panel-body" id="game-action-status">
                    <div class="row">
                        <div class="col-xs-5 cp-text-large text-right">Current round</div>
                        <div class="col-xs-7 cp-text-large" id="current-round-lbl"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 cp-text-large text-right">Time left</div>
                        <div class="col-xs-7 cp-text-large" id="current-round-lbl"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 cp-text-large text-right">Status</div>
                        <div class="col-xs-7 cp-text-large" id="current-round-lbl"></div>
                    </div>
                </div>
                <div class="panel-body">
                    <h4>Game action</h4>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="begin-round-time" class="col-sm-3 control-label">Begin round</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="number" class="form-control" id="begin-round-time" required placeholder="Time in minute">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" id="begin-round-btn" type="button">Go</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button class="btn btn-warning" id="stop-round-btn">Stop round</button>
                            <button class="btn btn-danger" id="finalize-round-btn">Finalize round</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Game status</h3>
                </div>
                <div class="panel-body" id="game-status-box">
                    <div id="game-status-save-box">
                        <button class="btn btn-success" id="game-status-save">Save</button>
                        <span class="warning-text">Your change is currently unsaved</span>
                    </div>
                </div>
                <div id="current-game-status"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Game management</h3>
                </div>
                <div id="current-game-list"></div>
                <div class="panel-body" id="game-mgmt-box">
                    <h4>Create new game</h4>
                    <form id="create-game-form" class="form-horizontal">
                        <div class="form-group">
                            <label for="new-game-name" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-9"><input type="text" class="form-control" id="new-game-name" name="name" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3"><button type="submit" class="btn btn-success" id="new-game-btn">Create</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Players</h3>
                </div>
                <div id="player-list"></div>
                <div class="panel-body">
                    <h4>Add players</h4>
                    <form id="add-player-form" class="form-horizontal">
                        <div class="form-group">
                            <label for="new-player-username" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9"><input type="text" class="form-control" id="new-player-username" name="username" required></div>
                        </div>
                        <div class="form-group">
                            <label for="new-player-password" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9"><input type="text" class="form-control" id="new-player-password" name="password" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3"><button type="submit" class="btn btn-success" id="new-player-btn">Create</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="edit-user-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit player</h4>
            </div>
            <div class="modal-body" id="edit-user-form-container">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="edit-player-username" class="col-sm-3 control-label">Username</label>
                        <div class="col-sm-9"><input type="text" class="form-control" id="edit-player-username" name="username" required></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-player-password" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9"><input type="text" class="form-control" id="edit-player-password" name="password" required></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-player-money" class="col-sm-3 control-label">Money</label>
                        <div class="col-sm-9"><input type="number" class="form-control" id="edit-player-money" name="money" required></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="edit-player-btn">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script id="template-edit-user-form" type="text/x-handlebars-template">

</script>

<script id="template-alert" type="text/x-handlebars-template">
    <div class="alert alert-{{type}} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{{text}}}
    </div>
</script>
<script id="template-game-list" type="text/x-handlebars-template">
    {{#if game}}
        <table class="table">
            <thead>
            <tr><th>#</th><th>Name</th><th>---</th></tr>
            </thead>
            <tbody>
            {{#each game}}
            <tr{{#if active}} class="success"{{/if}}><td>{{id}}</td><td>{{name}}</td>
                <td>
                    {{#unless active}}<button data-api="<?=$base?>api/admin/set_active_game" data-id="{{id}}" class="btn btn-xs btn-primary">Set active</button>{{/unless}}
                    <button data-api="<?=$base?>api/admin/reset_game" data-id="{{id}}" class="btn btn-xs btn-warning">Reset</button>
                    {{#unless active}}<button data-api="<?=$base?>api/admin/destroy_game" data-id="{{id}}" class="btn btn-xs btn-danger">Destroy</button>{{/unless}}
                </td></tr>
            {{/each}}
            </tbody>
        </table>
    {{else}}
        <div class="panel-body">No game current active.</div>
    {{/if}}
</script>
<script id="template-game-status" type="text/x-handlebars-template">
    <table class="table" id="game-status-table" data-id="{{id}}">
        <tr><th>Current round</th><td>{{round}}</td></tr>
        <tr><th>Distance</th><td><a href="#" class="editable" data-name="distance">{{distance}}</a></td></tr>
        <tr><th>Food</th><td><a href="#" class="editable" data-name="food">{{food}}</a></td></tr>
        <tr><th>HP</th><td><a href="#" class="editable" data-name="hp">{{hp}}</a></td></tr>
        <tr><th>Energy</th><td><a href="#" class="editable" data-name="power">{{power}}</a></td></tr>
        <tr><th>Internet</th><td><a href="#" class="editable" data-name="internet">{{internet}}</a></td></tr>
        <tr><th>Money</th><td><a href="#" class="editable" data-name="money">{{money}}</a></td></tr>
    </table>
</script>
<script id="template-player-list" type="text/x-handlebars-template">
    {{#if players}}
    <table class="table">
        <thead>
        <tr><th>#</th><th>User</th><th>Money</th><th>---</th></tr>
        </thead>
        <tbody>
        {{#each players}}
        <tr><td>{{id}}</td><td>{{username}}</td><td>{{money}}</td>
        <td>
            <button data-id="{{id}}" data-username="{{username}}" data-password="{{password}}" data-money="{{money}}" class="btn btn-xs btn-warning btn-edit-player">Edit</button>
            <button data-id="{{id}}" class="btn btn-xs btn-danger btn-destroy-player">Destroy</button>
        </td></tr>
        {{/each}}
        </tbody>
    </table>
    {{else}}
    <div class="panel-body">No player associated with game.</div>
    {{/if}}
</script>
<script id="template-game-action" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-xs-5 cp-text-large text-right">Current round</div>
        <div class="col-xs-7 cp-text-large" id="current-round-lbl">{{current_round}}</div>
    </div>
    <div class="row">
        <div class="col-xs-5 cp-text-large text-right">Time left</div>
        <div class="col-xs-7 cp-text-large" id="current-round-lbl">{{#if status}}{{time_text finished_time}}{{else}}Pending{{/if}}</div>
    </div>
    <div class="row">
        <div class="col-xs-5 cp-text-large text-right">Status</div>
        <div class="col-xs-7 cp-text-large" id="current-round-lbl">{{game_status_text status}}</div>
    </div>
</script>

<script>

    var CPGameAdm = {
        'status_dirty': false,
        'game_id': -1,
        'game_action_status': {
            current_round: 0,
            finished_time: 0,
            status: 0
        }
    };

    var _tmpl_alert = Handlebars.compile($('#template-alert').html());
    var _tmpl_game_list = Handlebars.compile($('#template-game-list').html());
    var _tmpl_player_list = Handlebars.compile($('#template-player-list').html());
    var _tmpl_game_status = Handlebars.compile($('#template-game-status').html());
    var _tmpl_game_action = Handlebars.compile($('#template-game-action').html());

    function create_alert(type, text) {
        return _tmpl_alert({'type': type, 'text': text});
    }

    function reloadGameData() {
        $.getJSON('<?=$base?>api/admin/list_game', function(res) {
            $('#current-game-list').html(_tmpl_game_list({'game': res}));

            // Re-attach event listener
            $('#current-game-list button').click(function() {
                var api = $(this).attr('data-api');
                var id = $(this).attr('data-id');
                if (!confirm('Are you sure you want to \'' + $(this).html() + '\'?'))
                    return;
                $.getJSON(api, {'game': id}).done(function() {
                    reloadGameData();
                    reloadActiveGameData();
                }).fail(function() {
                    alert('Something went wrong doing last action!');
                });
            });
        }).fail(function() {
            $(create_alert('danger', '<strong>Error!</strong> Unable to reach server.')).prependTo('#game-mgmt-box');
        });
    }

    function reloadActiveGameData() {
        $.getJSON('<?=$base?>api/admin/game_info', function(res) {
            $('#current-game-status').html(_tmpl_game_status(res));
            $('#player-list').html(_tmpl_player_list(res));

            CPGameAdm.game_id = res.id;

            // Game status listener
            $('#current-game-status .editable').on('save', function(e) {
                CPGameAdm.status_dirty = true;
                $('#game-status-box').show();
            }).editable({
                'mode': 'popup', // popup|inline
                'validate': function(v) {
                    if (!$.isNumeric(v))
                        return 'Number required!';
                }
            });

            // Player listener
            $('.btn-destroy-player').click(function() {
                var id = $(this).attr('data-id');
                if (!confirm('Are you sure you want to destroy user?'))
                    return;
                $.getJSON('<?=$base?>api/admin/destroy_player', {'user_id': id}).always(function() {
                    reloadActiveGameData();
                }).fail(function(){
                    alert('Fail to destroy player!');
                });
            });
            $('.btn-edit-player').click(function() {
                var id = $(this).attr('data-id');
                $('#edit-player-username').val($(this).attr('data-username'));
                $('#edit-player-password').val($(this).attr('data-password'));
                $('#edit-player-money').val($(this).attr('data-money'));
                $('#edit-player-btn').attr('data-id', id);
                $('#edit-user-modal').modal();
            });
        }).fail(function() {
            $(create_alert('warning', '<strong>Error!</strong> Game not found.')).prependTo('#game-status-box');
        });
    }

    function updateGameActionStatus() {
        $('#game-action-status').html(_tmpl_game_action(CPGameAdm.game_action_status));

        if (CPGameAdm.game_action_status.status == 0) {
            $('#stop-round-btn').prop('disabled', true);
            $('#finalize-round-btn').prop('disabled', true);
        } else {
            $('#stop-round-btn').prop('disabled', false);
            $('#finalize-round-btn').prop('disabled', false);
        }
    }

    setInterval(updateGameActionStatus, 1000);

    function fetchGameActionStatus() {
        $.getJSON('<?=$base?>api/admin/get_update', function(res) {
            CPGameAdm.game_action_status.finished_time = +res.finished_time;
            CPGameAdm.game_action_status.status = +res.status;
            CPGameAdm.game_action_status.current_round = +res.round;
        }).always(function() {
            setTimeout(fetchGameActionStatus, 1500);
        }).fail(function() {
            console.error('Fail to fetch status!');
        });
    }

$(function() {
    reloadGameData();
    reloadActiveGameData();
    fetchGameActionStatus();

    $('#create-game-form').submit(function(e) {
        e.preventDefault();

        var txt = $('#new-game-name');
        var btn = $('#new-game-btn');

        txt.prop('disabled', true);
        btn.prop('disabled', true);
        btn.html('Creating...');

        var server_data = {'name': txt.val()};
        $.getJSON('<?=$base?>api/admin/create_game', server_data)
            .fail(function() {
                $(create_alert('danger', '<strong>Error!</strong> Please try again')).prependTo($('#create-game-form'));
            })
            .done(function() {reloadGameData();txt.val('');})
            .always(function() {
                txt.prop('disabled', false);
                btn.prop('disabled', false);
                btn.html('Create');
            });
    });

    $('#game-status-save').click(function() {
        var data = {};
        $('#current-game-status .editable').each(function(o) {
            var x = $(this);
            data[x.attr('data-name')] = x.html();
        });
        data.game = $('#game-status-table').attr('data-id');
        $(this).prop('disabled', true).html('Saving...');
        $.getJSON('<?=$base?>api/admin/update_game', data)
            .always(function() {
                $('#game-status-save').prop('disabled', false).html('Save');
            }).fail(function() {
                $(create_alert('danger', '<strong>Error!</strong> Unable to save.')).prependTo($('#current-game-status'));
            }).done(function() {
                reloadActiveGameData();
                $('#game-status-box').hide();
            });
    });

    $('#add-player-form').submit(function(e) {
        e.preventDefault();

        var btn = $('#new-player-btn');
        btn.prop('disabled', true).html('Creating...');

        var server_data = {
            'game': CPGameAdm.game_id,
            'username': $('#new-player-username').val(),
            'password': $('#new-player-password').val()
        };
        $.getJSON('<?=$base?>api/admin/create_player', server_data)
            .fail(function() {
                $(create_alert('danger', '<strong>Error!</strong> Please try again')).prependTo($('#add-player-form'));
            })
            .done(function() {
                reloadActiveGameData();
                $('#new-player-username').val('');
                $('#new-player-password').val('');
            }).always(function() {
                btn.prop('disabled', false).html('Create');
            });
    });

    $('#begin-round-btn').click(function() {
        var dat = {
            'time': $('#begin-round-time').val(),
            'game_id': CPGameAdm.game_id
        };

        $.getJSON('<?=$base?>api/admin/begin_round', dat).fail(function() { alert('Fail to begin round!'); });
    });

    $('#stop-round-btn').click(function() {
        var dat = {
            'game_id': CPGameAdm.game_id
        };

        $.getJSON('<?=$base?>api/admin/stop_round', dat).fail(function() { alert('Fail to stop round!'); });
    });

    $('#finalize-round-btn').click(function() {
        var dat = {
            'game_id': CPGameAdm.game_id
        };

        $.getJSON('<?=$base?>api/admin/finalize_round', dat).done(function() {
            reloadActiveGameData();
        }).fail(function() { alert('Fail to finalize round!'); });
    });

    $('#edit-player-btn').click(function() {
        var btn = $(this);
        btn.html('Saving...').prop('disabled', true);
        var data = {
            username: $('#edit-player-username').val(),
            password: $('#edit-player-password').val(),
            money: $('#edit-player-money').val(),
            user_id: btn.attr('data-id')
        };
        $.getJSON('<?=$base?>api/admin/update_player', data, function() {
            $('#edit-user-modal').modal('hide');
            reloadActiveGameData();
        }).fail(function() {
            $(create_alert('danger', '<strong>Error!</strong> Cannot save changes.')).prependTo($('#edit-user-form-container'));
        }).always(function() {
            btn.html('Save changes').prop('disabled', false);
        });
    });
});
</script>
</body>
</html>
