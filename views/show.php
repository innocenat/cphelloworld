<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Let's go to Pluto!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.3/handlebars.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <link rel="stylesheet" href="<?=$base?>cp.css">
    <script src="<?=$base?>js/view.helpers.js?v1"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body{background: #111 url(<?=$base?>img/pluto.png) no-repeat center center;
            color:#fff; font-size: 16pt;}
        .tfin{background-color: #ff0;color:#000}
        .container-fluid{padding:50px;padding-bottom: 0}
        .dist {width:100%;}
        .dist td{width: 7.7%; height:50px; text-align: center; border: 3px solid #666;}
        .dist .dist-label td{border: none;}
        .status {width:100%;}
        .status td{height:40px; text-align: center; border: 3px solid #666;}
        .money-pool{width:100%; height:180px; text-align: center; background-color:#666; border-radius: 10px; padding:20px;}
        #money{font-size:36pt;}
        .slabel {width:70px;}
        .rt {width:100%; height:140px; text-align: center; margin-bottom:20px; background-color:#666; border-radius: 10px; padding:20px;}
        .rt span{font-size:30pt;}
        .rt h4{font-size:16pt;}

        #tr-pos-0 { border-bottom: 5px solid #fff }

        #ship-img {width:70px}
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12" style="text-align: center">
            <h1 style="border:none">CP Mega Project</h1>
            <table class="dist">
                <tr>
                    <td id="cell-dist-m2"></td>
                    <td id="cell-dist-m1"></td>
                    <td id="cell-dist-0"></td>
                    <td id="cell-dist-1"></td>
                    <td id="cell-dist-2"></td>
                    <td id="cell-dist-3"></td>
                    <td id="cell-dist-4"></td>
                    <td id="cell-dist-5"></td>
                    <td id="cell-dist-6" class="tfin"></td>
                    <td id="cell-dist-7" class="tfin"></td>
                    <td id="cell-dist-8" class="tfin"></td>
                    <td id="cell-dist-9" class="tfin"></td>
                    <td id="cell-dist-10"></td>
                </tr>
                <tr class="dist-label">
                    <td>-2</td>
                    <td>-1</td>
                    <td>0</td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td>6</td>
                    <td>7</td>
                    <td>8</td>
                    <td>9</td>
                    <td>10</td>
                </tr>
            </table>
        </div>
        <div class="col-sm-8">
            <canvas id="graph-canvas" width="550px" height="510px">
            </canvas>
        </div>
        <div class="col-sm-4">
            <div class="rt">
                <div class="col-xs-6">
                    <h4>ROUND</h4>
                    <span id="round"></span>
                </div>
                <div class="col-xs-6">
                    <h4>TIME</h4>
                    <span id="time">-</span>
                </div>
            </div>
            <div class="money-pool">
                <h2>Money Pool</h2>
                <span id="money">12345</span>
            </div>
            <div class="party">
                <h3>Pluto Party</h3>
                <ul>
                    <li>6 &le; Distance &le; 9</li>
                    <li>Net speed &ge; 5</li>
                    <li>Money pool &ge; 70,000</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>

    var StatsStr = [];
    var TimeS = {status:0, finished_time: 0};
    var ctx;

    function refresh() {

        $.getJSON('<?=$base?>show/json', function(res) {
            var newStats = [res.food, res.hp, res.power, res.internet];

            // Distance
            $('.dist tr:first td').html('');
            var cell_id = 'cell-dist-';
            var dist = Math.max(-2, Math.min(10, res.distance));
            if (dist < 0) cell_id += 'm' + Math.abs(dist);
            else cell_id += dist;
            $('.dist td#' + cell_id).html('<img src="<?=$base?>img/ship.png" id="ship-img">');

            if (res.distance < -2) {
                $('#cell-dist-m1').html('<div style="font-size:130%;font-weight:bold;color:yellow">@' + res.distance + '</div>');
            }
            if (res.distance > 10) {
                $('#cell-dist-9').html('<div style="font-size:130%;font-weight:bold;color:red">@' + res.distance + '</div>');
            }

            // Stats graph
            if ((newStats+"") != (StatsStr+"")) {
                StatsStr = newStats;
                drawGraph(ctx, StatsStr);
                console.log("Drawn");
            }

            // Money
            $('#money').html(res.money);

            // Round
            $('#round').html(res.round);

            // Time
            TimeS.status = res.status;
            TimeS.finished_time = res.finished_time;

        }).always(function() {
            setTimeout(refresh, 1000);
        });
    }

    function updateTime() {
        var cT = moment().unix();
        if (TimeS.status == 0 || cT >= TimeS.finished_time) {
            $('#time').html('--');
        } else {
            var ll = TimeS.finished_time - cT;
            var cm = Math.floor(ll/60);
            var cs = ll%60;
            if (cs < 10) cs = "0" + cs;
            var str = cm + ":" + cs;
            $('#time').html(str);
        }
    }

    $(function() {
        ctx = document.getElementById('graph-canvas').getContext('2d');
        refresh();
        setInterval(updateTime, 1000);
    });

    function drawGraph(ctx, data) {
        ctx.clearRect(0, 0, 1024,768);


        // Draw axis label
        ctx.textAlign = "right";
        ctx.font = "20px sans-serif";
        ctx.fillStyle="white";
        for (var i = 0; i < 10; i += 2) {
            ctx.fillText(i, 23, 400-40*i+8);
        }

        // Draw aux. axis
        ctx.strokeStyle = "rgba(255, 255, 255, 0.5)";
        ctx.lineWidth = 1;
        ctx.beginPath();
        for (var i = 1; i < 12; i++) {
            ctx.moveTo(30, 40*i);
            ctx.lineTo(640, 40*i);
        }
        ctx.stroke();

        // Draw another label
        ctx.fillStyle = "white";
        ctx.textAlign = "center";
        ctx.fillText("Food", 100, 505);
        ctx.fillText("HP", 230, 505);
        ctx.fillText("Energy", 360, 505);
        ctx.fillText("Internet", 490, 505);


        // Draw bar
        ctx.fillStyle = "#fff";
        for (var i = 0; i < 4; i++) {
            var v = Math.min(10, Math.max(-2, data[i]));
            if (data[i] >= 0) {
                var vv = 255 - Math.floor(255 * Math.min(10, data[i]) / 10);
                ctx.fillStyle = "rgb(" + vv + ",255," + vv + ")";
            } else {
                var vv = 255 - Math.floor(255 * Math.min(3, Math.abs(data[i])) / 3);
                ctx.fillStyle = "rgb(255," + vv + "," + vv + ")";
            }
            ctx.fillRect(40+130*i,400-40*Math.max(0,v),120,Math.abs(40*v));
        }

        // Draw bar label
        ctx.fillStyle="black";
        ctx.textAlign = "center";
        for (var i = 0; i < 4; i++) {
            var offset = 24;
            if (data[i] == 0.5 || data[i] == -0.5) {
                ctx.font = "14px sans-serif";
                offset = 14;
            } else {
                ctx.font = "24px sans-serif";
            }
            var v = Math.min(10, Math.max(-2, data[i]));
            ctx.fillText(data[i], 40+130*i+60, 400-40*Math.max(0,v)+offset);
        }

        // Pluto party threshold
        ctx.strokeStyle = "rgba(255,255,0,1)";
        ctx.lineWidth = 5;
        ctx.beginPath();
        ctx.moveTo(425, 200);
        ctx.lineTo(640, 200);
        ctx.stroke();

        // Draw axis
        ctx.strokeStyle = "rgba(255, 255, 255, 1)";
        ctx.lineWidth = 5;
        ctx.beginPath();
        ctx.moveTo(30, 0);
        ctx.lineTo(30, 480);
        ctx.moveTo(30, 400);
        ctx.lineTo(640, 400);
        ctx.stroke();
    }
</script>
</body>
</html>
