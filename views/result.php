<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Celestial Pluto - Hello CP World</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?=$base?>cp.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Celestial Pluto - Result Round #<?=$round?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h2>Engine</h2>
            <table class="table">
                <thead><tr><th>Engine</th><th>Money</th></tr></thead>
                <tbody>
                <?php
                foreach ($result[0]->engine as $k=>$v) {
                    echo "<tr", ((in_array($k, $result[1][2]->engine)) ? " class=\"success\"" : "") ,"><td>Engine #", $k+1, "</td><td>$v</td></tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <h2>Auction</h2>
            <table class="table">
                <thead><tr><th>Auction</th><th>Money/Vote</th></tr></thead>
                <tbody>
                <?php
                foreach ($result[0]->auction as $k=>$v) {
                    echo "<tr", ((in_array($k, $result[1][2]->auction)) ? " class=\"success\"" : "") ,"><td>Auction #", $k+1, "</td><td>$v</td></tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <h2>Item</h2>
            <table class="table">
                <thead><tr><th>Item</th><th>Count</th></tr></thead>
                <tbody>
                <?php
                foreach ($result[0]->choose as $k=>$v) {
                    echo "<tr", ((in_array($k, $result[1][2]->choose)) ? " class=\"success\"" : "") ,"><td>Item #", $k+1, "</td><td>$v</td></tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h2>Money stuff</h2>
            Pay: <?= $result[0]->pay_sum ?><br>
            Pool: <?= $result[0]->pool_amnt ?><br>
        </div>
        <div class="col-md-4">
            <h2>Stats change</h2>
            <table class="table">
                <thead><tr><th>Item</th><th>Change</th></tr></thead>
                <tbody>
                    <tr><td>Distance</td><td><?=$result[1][1]->distance?></td></tr>
                    <tr><td>Food</td><td><?=$result[1][1]->food?></td></tr>
                    <tr><td>HP</td><td><?=$result[1][1]->hp?></td></tr>
                    <tr><td>Energy</td><td><?=$result[1][1]->power?></td></tr>
                    <tr><td>Internet</td><td><?=$result[1][1]->internet?></td></tr>
                </tbody>
            </table>
        </div> <div class="col-md-4">
            <h2>Money paid</h2>
            <table class="table">
                <thead><tr><th>Value</th></tr></thead>
                <tbody>
                <?php foreach ($money as $m): ?>
                <tr><td><?=$m?></td></tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
